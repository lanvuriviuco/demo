const { Int32 } = require('mongodb');
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AccountSchema = new Schema({
    uuid: { type: String, unique: true, required: true, trim: true },
    state: { type: String },
    name: { type: String },
    phone: { type: String },
    email: { type: String, unique: true, trim: true },
    username: { type: String, required: true, trim: true, minlength: 2 },
    birthday: { type: Number },
    avatar_link: { type: String },
    user_type: { type: String, maxlength: 20 },
    email: { type: String },
    create_date: { type: Number },
    write_date: { type: Number },

}, { collection: 'account' });

module.exports = mongoose.model('account', AccountSchema)
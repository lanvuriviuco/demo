//lets require/import the mongodb native drivers.
var mongodb = require('mongodb');

//We need to work with "MongoClient" interface in order to connect to a mongodb server.
var MongoClient = mongodb.MongoClient;

var database_name = 'demo_travel';
var username = 'lanvu96';
var password = 'Q3MDj2CAuArbnp4';

// Connection URL. This is where your mongodb server is running.
const uri = `mongodb+srv://${username}:${password}@demo-travel.gxoxr.mongodb.net/${database_name}?retryWrites=true&w=majority`;

var client = new MongoClient(uri);

class MongoClass {
    constructor(o = {}) {
        this.client = client;
        this.collection_name = o['collection_name']
    }

    async findByUuid(uuid) {
        try {
            await this.client.connect()
            const result = await this.client.db(database_name).collection(this.collection_name).findOne({ uuid: uuid });
            if (result) {
                console.log(`Found a listing in the collection with the uuid '${uuid}':`);
                return result
            } else {
                console.log(`No listings found with the uuid '${uuid}'`);
            }
        } catch (error) {
            
        } finally {
            this.client.close();
        }
        
    }

}

module.exports = MongoClass;
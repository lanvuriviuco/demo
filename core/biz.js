

module.exports = class {
    constructor(o = {}) {
        this._model = o['model'];
        this._schema = require(`../mongoSchema/${this._model}`);
    }

    get schema() {
        return this._schema;
    }

    async _beforeResModel(data) { return data; }

    async findByUuid(uuid) {
        let rs = await this._schema.findOne({ uuid }).lean();
        return rs ? rs : null;
    }

    async create(data) {
        return this._schema.create({ ...data });
    }

    async deleteByUuid(uuid) {
        return this._schema.deleteOne({ uuid });
    }

    async updateByUuid(uuid, data) {
        await this._schema.updateOne({ uuid }, { ...data });
        return this.findByUuid(uuid)
    }

    async find({ condition = {}, sort = {}, page, page_size } = {}) {
        let query = this._schema.find(condition).lean();

        // adding sort
        const { sort_by, sort_order = 'desc' } = sort;
        if(sort_by) query.sort({ [sort_by]: sort_order })


        query.skip(parseInt(page_size * page)).limit(parseInt(page_size))
        return query.exec();
    }

    async count({ condition }) {
        return this._schema.countDocuments({ ...condition })
    }

    async list({ page = 1, page_size = 10, condition = {}, sort = {} } = {}) {

        let arr = await this.find({ condition, sort, page, page_size });
        let total = await this.count({ condition });

        return {
            total,
            items: arr.length > 0 ? arr : []
        }
    }
}

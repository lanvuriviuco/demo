const path = require('path');
const mongoose = require('mongoose');
const  { Config } = require(path.join(global.root_path || '../', 'config', 'index'));

var database_name = Config.mongo_db.name;
var username = Config.mongo_db.username;
var password = Config.mongo_db.password;

// Connection URL. This is where your mongodb server is running.
const uri = `mongodb+srv://${username}:${password}@demo-travel.gxoxr.mongodb.net/${database_name}?retryWrites=true&w=majority`

// set mongoose Promise to Bluebird
mongoose.Promise = Promise;

// Exit application on error
mongoose.connection.on('error', (err) => {
  console.log(`MongoDB connection error: ${err}`);
  process.exit(-1);
});

// print mongoose logs in dev env

/**
 * Connect to mongo db
 *
 * @returns {object} Mongoose connection
 * @public
 */
exports.connect = () => {
  mongoose
    .connect(uri, {
      useCreateIndex: true,
      keepAlive: 1,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    })
    .then(() => console.log('mongoDB connected...'));
  return mongoose.connection;
};
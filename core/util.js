const path = require('path');
const fs = require('fs');

class Util {
    static isFile(f) {
        let stat = fs.lstatSync(f);
        return stat.isFile();
    }

    static loadFilesDeepDir(dir, ignores = []) {
        let files = fs.readdirSync(dir);
        return files.reduce((files, f) => {
            let fPath = path.join(dir, f);
            if (Util.isFile(fPath))
                return ignores && !ignores.includes(fPath) ? [...files, fPath] : files;
            return [...files, ...Util.loadFilesDeepDir(fPath)];
        }, []);
    }
}

module.exports = Util
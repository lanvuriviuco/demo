const { Joi } = require('core-api')

module.exports = ({ server }) => {

    server.api.register({
        method: 'post',
        path: '/test',
        tags: ['Test'],
        parameter: {
            name: Joi.string().required().default('Lân Vũ').description('name'),
            phone: Joi.string().required().description('phone'),
            email: Joi.string().required().description('email'),
        },
        response: Joi.object({
            name: Joi.string(),
            phone: Joi.string(),
            email: Joi.string(),
        }),
        handler: async ({ args }) => {
            return {
                ...args
            };
        },
    });

    server.api.register({
        method: 'get',
        path: '/user/account',
        tags: ['User Account'],
        middleware: {
            'paging-response': true
        },
        parameter: {
            sort_by: Joi.string().description('field'),
            sort_order: Joi.string().valid('asc', 'desc').description('field'),
            uuid: Joi.string().description('name'),
            state: Joi.string().description('name'),
        },
        response: Joi.array().items(Joi.object({
            uuid: Joi.string().description('uuid'),
            state: Joi.string().description('state'),
            name: Joi.string().description('name'),
            bio: Joi.string().description('bio'),
            username: Joi.string().description('username'),
            custom_name: Joi.string().description('custom_name'),
            phone: Joi.string().description('phone'),
            birthday: Joi.number().description('birthday'),
            gender: Joi.string().description('gender'),
            avatar_link: Joi.string().description('avatar_link'),
        })),
        handler: async ({ args }) => {
            const BizClass = require('../../core/biz');
            const Account = new BizClass({ model: 'account' });

            let { page, page_size, uuid, sort_by, sort_order } = args;
            console.log(args)
            let condition = {}, sort = {}
            if(uuid) condition['uuid'] = uuid
            if(sort_by) sort = { sort_by, sort_order }

            let { total, items } = await Account.list({ page, page_size, condition, sort });
            return {
                total, 
                items
            }
        },
    });

    server.api.register({
        method: 'get',
        path: '/user/account/:uuid',
        tags: ['User Account'],
        parameter: {
            uuid: Joi.string().meta({ location: 'path' }).description('name')
        },
        response: Joi.object({
            uuid: Joi.string().description('uuid'),
            state: Joi.string().description('state'),
            name: Joi.string().description('name'),
            bio: Joi.string().description('bio'),
            username: Joi.string().description('username'),
            custom_name: Joi.string().description('custom_name'),
            phone: Joi.string().description('phone'),
            birthday: Joi.number().description('birthday'),
            gender: Joi.string().description('gender'),
            avatar_link: Joi.string().description('avatar_link'),
        }),
        handler: async ({ args }) => {
            const BizClass = require('../../core/biz');
            const Account = new BizClass({ model: 'account' }) 

            let detail = await Account.findByUuid('65e234fbed9a4a3ea3dadc7fd9c8d449');
            return detail
        },
    });

}
const { Joi } = require('core-api')
const fs = require('fs');

module.exports = ({ server }) => {

    server.api.register({
        method: 'post',
        path: '/user/media/image',
        tags: ['User Media'],
        middleware: {
            'upload-file': {
                mode: 'array',
                fieldname: 'image_file',
                limit_file_size: 10 * 1024 * 1024,
                allow_types: ["png", "jpg", 'jpeg']
            },
        },
        response: Joi.array().items(Joi.object({
            uuid: Joi.string().description('mã định danh'),
            media_link: Joi.string().description('link media'),
            name: Joi.string().description('Name'),
            width: Joi.number(),
            height: Joi.number()
        })),
        handler: async ({ args }) => {
            let { files } = args;
            console.log(files)
            // let img = fs.readFileSync(files[0].path);
            // let encode_image = img.toString('base64');
            // console.log(encode_image)
            // fs.writeFile(__dirname, files[0].filename, function (err, data) {
            //     if (err) {
            //         return console.log(err);
            //     }
            //     console.log(data);
            // });
            return []
        },
    });

}
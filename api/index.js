const path = require('path');
const ignores = [path.join(__dirname, 'index.js')];
const Util = require(path.resolve(`${__dirname}/../core`, 'util'));

module.exports = ({ server, options }) => {

    let files = Util.loadFilesDeepDir(__dirname, ignores)
    try {
        files.map(f => {
            require(f)({ server })
        })
    } catch (error) {
        console.log(error)
    }
    
    // //joining path of directory 
    // const directoryPath = path.join(__dirname, '../', 'api');
    // let lstFile = []
    // //passsing directoryPath and callback function
    // fs.readdir(directoryPath, function (err, folders) {
    //     //handling error
    //     if (err) {
    //         return console.log('Unable to scan directory: ' + err);
    //     }
    //     folders = folders.filter(el => el != 'index.js')
    //     //listing all files using forEach
    //     folders.forEach(function (folder) {
    //         // Do whatever you want to do with the file
    //         fs.readdir(path.join(__dirname, folder), function(error, files) {
    //             let stat = fs.lstatSync(files);
    //             console.log(stat.isFile())
    //             console.log(files)
    //         });
    //         // lstFile.push(path.join(__dirname, f))
    //     });
    // });
    // console.log(lstFile)
}
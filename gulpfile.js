const gulp = require('gulp');
const nodemon = require('gulp-nodemon');

gulp.task('prod', () => {
    nodemon({
        exec: 'node',
        script: 'app-single.js',
        ext: 'js json',
        watch: ["config/*", "route/*", "models/*", "core/*", "biz/*", "db/*", "bootstrap/*", "middleware/*"],
        env: { 'NODE_ENV': 'production', 'TZ': 'UTC', "NODE_TLS_REJECT_UNAUTHORIZED": 0, "REDIS_NODE": 1 }
    })
});

// chạy local
gulp.task('lanvu', (done) => {
    nodemon({
        exec: 'node',
        script: 'app.js',
        ext: 'js',
        env: { 'NODE_ENV': 'development' },
        tasks: ['lint'],
        watch: ["config/*", "route/*", "models/*", "core/*", "biz/*", "db/*", "bootstrap/*", "middleware/*"],
        env: {'NODE_ENV': 'development', 'TZ': 'UTC', "NODE_TLS_REJECT_UNAUTHORIZED":0, "REDIS_NODE": 1}
    });
});


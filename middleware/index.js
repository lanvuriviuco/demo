module.exports = ({ server }) => {
    // đăng ký các middleware
    // require('./user-access-token')({ server });
    // require('./admin-access-token')({ server });
    // require('./current-location')({server});
    // require('./optional-user-access-token')({ server })

    // sắp xếp thứ tự các middleware
    server.api.middleware.workflow = [
        { key: 'prepare-meta', args: true },
        { key: 'upload-file', args: false },
        { key: 'parse-parameter', args: true },
        { key: 'user-access-token', args: false },
        { key: 'optional-user-access-token', args: false },
        { key: 'end-point-handler', args: true },
        { key: 'paging-response', args: false },
        { key: 'send-response', args: true },
    ];
};
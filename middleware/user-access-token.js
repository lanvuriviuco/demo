const { Joi } = require('core-api');

module.exports = ({ server }) => {
    server.api.middleware.register({
        key: 'user-access-token',
        summary: 'chứng thực người dùng',
        setup: ({ endPoint }) => {
            endPoint.parameter.token = Joi
                .string()
                .required()
                .meta({ location: 'header' })
                .description('token dùng để chứng thực');

            endPoint.errors.push('invalid-token');
        },
        handler: async ({ config, context }) => {
            // let { token } = context.args;            
            // let tokenData = JWT.verify(token, config.get('JWT_SECRET_KEY'));
            // const userLoginBiz = require(biz.userLogin)
            // let { new_access_token } = await userLoginBiz({
            //     lixiapp_token: token
            // });
            // let tokenData = JWT.verify(new_access_token, config.get('JWT_SECRET_KEY'));
            // if (!tokenData.lixibook_account_uuid || !tokenData.lixiapp_user_id)
            //     throw AppError({ code: 'invalid-token' });
            // context.user = {
            //     "lixibook_account_uuid": tokenData.lixibook_account_uuid,
            //     "lixiapp_user_id": tokenData.lixiapp_user_id};
        }
    });
};
const fs = require('fs');
const path = require('path');
const root_path = path.join(__dirname, '..');

// let lstDirBiz = fs.readdirSync(path.join(root_path, 'biz'));
// let exclude = ['init.js'];
// lstDirBiz = lstDirBiz.filter(fileName => exclude.indexOf(fileName) < 0 ? true : false);
// let lstFileBiz = [];

// for (let _dir of lstDirBiz) {
//     let _files = fs.readdirSync(path.join(root_path, 'biz', _dir));
//     _files = _files.map(file => path.join(root_path, 'biz', _dir, file));
//     lstFileBiz = lstFileBiz.concat(_files);
// }
// var BIZ = {};

// function getFileName(_path) {
//     let _file_name = path.basename(_path).replace('.js', '');
//     let _splits = _file_name.split(/[-_]/);
//     if (_splits.length === 0) {
//         return _file_name;
//     }
//     for (let _i in _splits) {
//         if (parseInt(_i) === 0) continue;
//         let _firtLetter= _splits[_i][0];
//         _splits[_i] = _splits[_i].replace(_firtLetter, _firtLetter.toUpperCase());
//     }
//     return _splits.join('');
// }

// function getFileNameModels(_path) {
//     let _file_name = path.basename(_path).replace('.js', '');
//     let _splits = _file_name.split(/[-_]/);
//     for (let _i in _splits) {
//         let _firtLetter= _splits[_i][0];
//         _splits[_i] = _splits[_i].replace(_firtLetter, _firtLetter.toUpperCase());
//     }
//     return _splits.join('');
// }

// for (let _file of lstFileBiz) {
//     let _fileName= getFileName(_file);
//     BIZ[_fileName] = _file;
// }
// /////Model

// let lstDirModel = fs.readdirSync(`${root_path}/models/`);
// lstDirModel = lstDirModel.filter( (fileName)=> {return exclude.indexOf(fileName) < 0 ? true : false;});
// let lstFileModel= [];
// for (let _dir of lstDirModel) {
//     let _files = fs.readdirSync(`${root_path}/models/${_dir}`);
//     _files = _files.map((file)=> {return `${root_path}/models/${_dir}/${file}`;});
//     lstFileModel = lstFileModel.concat(_files);
// }

// var MODEL = {};

// for (let _file of lstFileModel) {
//     let _fileName = getFileNameModels(_file);
// //     MODEL[_fileName] = _file;
// }

module.exports = {
    // 'models': MODEL,
    'core': {
        'elastic': `${root_path}/core/elastic.js`,
    },
    // 'biz': BIZ
};



module.exports = async ({
    uuid
}) => {
    const mongoClass = require('../core/mongo');
    const accountBiz = new mongoClass({ collection_name: 'account' })

    let account = await accountBiz.findByUuid(uuid)

    return account;
}
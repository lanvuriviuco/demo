const path = require('path');
const { Joi, ServerConfig } = require('core-api');
const  { Config } = require(path.join(global.root_path || '../', 'config', 'index'));

const config = new ServerConfig();
// define một config mới
config.define({
    key: 'JWT_SECRET_KEY',
    value: Config.secret,
    schema: Joi.string().required().description('secret key để khởi tạo và xác thức token')
});

config.define({
    key: 'ADMIN_JWT_SECRET_KEY',
    value: Config.jwt_secret_key,
    schema: Joi.string().required().description('secret key để khởi tạo và xác thức token cua admin')
});

// Override app port
config.set({ key: 'API_PORT', value: Config.port });
config.define({
    key: 'IP',
    value: Config.ip,
    schema: Joi.string().required().description('App ip')
});
 
module.exports = config;

const path = require('path');
const { Server, Joi } = require('core-api');
const mongoose = require('../core/mongoose');
// const Util = require(path.join(global.root_path, 'core', 'util'));
// const { models, core, biz } = require(path.join(global.root_path, 'const', 'handler'));

module.exports = async () => {

    const server = new Server({
        config: require('./config'),
    });
    // const options = {
    //     models,
    //     core,
    //     biz
    // };

    // register middleware
    require('../middleware')({ server });

    //setup api routes
    require('../api')({ server });


    server.api.register({
        method: 'get',
        path: '/check_live',
        tags: ['default'],
        response: Joi.string(),
        handler: async ({ args }) => {
            return 'API is ok!'
        },
    });

    mongoose.connect();
    // start server
    server.startup();

    // Log information
    console.log(`Worker ${process.pid} started tz: ${process.env.TZ} env: ${process.env.NODE_ENV} tls: ${process.env.NODE_TLS_REJECT_UNAUTHORIZED}`);
};
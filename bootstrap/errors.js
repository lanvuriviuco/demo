const { ErrorList } = require('nodejs-service-framework');

const errors = new ErrorList();

// nạp danh sách các lỗi
errors.load([
  {
    code: 'invalid-token',
    default: 'invalid token'
  },
  {
    code: 'not-found',
    default: 'We could not find what you want'
  },
  {
    code: 'not-legal',
    default: 'That operation is not valid'
  },
  {
    code: 'body-length-reach-limit',
    default: 'The review\'s body is too long'
  },
  {
    code: 'already-exists',
    default: 'That operation is already existed'
  },
  {
    code: 'editor-can-not-publish',
    default: 'This review is already published',
  },
  {
    code: 'admin-can-not-publish',
    default: 'This review is invalid to publish',
  },
  {
    code: 'account-not-found',
    default: 'Account not found',
  },
  {
    code: 'create-failed',
    default: 'Error in creating action. Please try again later'
  },
  {
    code: 'update-failed',
    default: 'Error in updating action. Please try again later'
  },
  {
    code: 'cannot-sort-by-near-by',
    default: 'Can not sort by distance due to invalid parameters',
  },
  {
    code: 'event-not-available',
    default: 'This Event is not avaible now',
  },
  {
    code: 'bad-request',
    default: 'bad-request',
  }
]);

// nạp danh sách các lỗi tiếng việt
errors.load([
  {
    code: 'invalid-token',
    'vi-vn': 'token không hợp lệ hoặc bị hết hạn'
  },
  {
    code: 'not-found',
    'vi-vn': 'Không tìm thấy đối tượng khả dụng'
  },
  {
    code: 'not-legal',
    'vi-vn': 'Thao tác vừa rồi không hợp lệ. Xin vui lòng thử lại sau'
  },
  {
    code: 'already-exists',
    'vi-vn': 'Thao tác vừa rồi đã tồn tại'
  },
  {
    code: 'body-length-reach-limit',
    'vi-vn': 'Bài viết đã vượt quá giới hạn cho phép'
  },
  {
    code: 'editor-can-not-publish',
    'vi-vn': 'Bài viết đã được đăng bởi chủ sở hữu',
  },
  {
    code: 'admin-can-not-publish',
    'vi-vn': 'Trạng thái bài viết không hợp lệ',
  },
]);

module.exports = errors;